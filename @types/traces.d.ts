declare interface TracerPath {
    name: string
    path: string
}

declare interface I18nTracerList {
    ja: TracerPath[],
    zh: TracerPath[]
}

declare interface Tracer {
    pict: string
    answer: string
}

declare interface TracerFile {
    name: string
    file: Tracer[]
}