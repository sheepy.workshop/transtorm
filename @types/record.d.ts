declare interface StormCard {
    No: number
    word: string
    ori: string
    src: "ja" | "zh"
    tgt: "zh" | "ja"
    author: string
    comment?: string
    randamize: number
}