import { defineNuxtConfig } from 'nuxt3'

// https://v3.nuxtjs.org/docs/directory-structure/nuxt.config
export default defineNuxtConfig({
    meta: {
        htmlAttrs: {
            prefix: "og: http://ogp.me/ns#",
        },
        script: [
            {
                children: `
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', '${process.env.GTAG}');
                `
            },
            {
                src: `https://www.googletagmanager.com/gtag/js?id=${process.env.GTAG}`,
                async: true
            },
        ]
    },
    router: {
        scrollBehavior: () => {
            return { top: 0 }
        }
    },
    css: [
        // 'bulma'
        "~/assets/styles/app.scss"
    ],
    publicRuntimeConfig: {
        storm: "transtomer",
        table_id: "tblSRvpF66jsZ40qG",
        site: "https://www.transtormer.work",
    },
    privateRuntimeConfig: {
        deepl: process.env.DEEPL_APIKEY,
        microcms: process.env.MICROCMS_APIKEY,
        airtable: process.env.AIRTABLE_APIKEY,
        gtag: process.env.GTAG
    }
})
