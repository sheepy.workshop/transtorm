const tracers: I18nTracerList = {
    ja: [
        {
            name: "櫻の樹の下には（梶井基次郎）",
            path: "sakura_no_ki_no_shitaniha"
        },
        // {
        //     name: "檸檬（梶井基次郎）",
        //     path: "remon"
        // },
        // {
        //     name: "夜長姫と耳男（坂口安吾）",
        //     path: "yonagahime_to_mimio"
        // }
    ],
    zh: []
}

export const useTracers = () => {
    return useState<I18nTracerList>("tracers", () => tracers)
}