import type { Ref } from "vue"
export const updateText = (text: Ref<string>) => (update: string) => { text.value = update }

export const useMyText = () => {
    const text = useState<string>("shared", () => "")
    return {
        original: readonly(text),
        update: updateText(text)
    }
}