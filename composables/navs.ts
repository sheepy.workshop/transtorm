export interface NavItem {
  ja: string
  zh: string
  href: string
}

export interface NavCategory {
  ja: string
  zh: string
  items: NavItem[]
  top: string
}

const navs: NavCategory[] = [
  {
    ja: "DeepLとお勉強",
    zh: "DeepLとお勉強",
    top: "/study",
    items: [
      {
        ja: "日本語 → 中国語",
        zh: "日文 → 中文",
        href: "/ja2zh"
      },
      {
        ja: "中国語 → 日本語",
        zh: "中文 → 日文",
        href: "/zh2ja"
      },
    ]
  },
  {
    ja: "写経",
    zh: "临摹",
    top: "/trace",
    items: [
      {
        ja: "自由課題",
        zh: "自由課題",
        href: "/freetheme"
      },
      {
        ja: "日本語",
        zh: "日文",
        href: "#ja"
      },
      {
        ja: "中国語",
        zh: "中文",
        href: "#zh"
      },
    ]
  },
  {
    ja: "My 用語集",
    zh: "我的词汇库",
    top: "/storm",
    items: [
      {
        ja: "ランダム用語を見る",
        zh: "随机查看",
        href: "/cards"
      },
      {
        ja: "用語登録",
        zh: "新建词汇",
        href: "/register"
      }
    ]
  },
  {
    ja: "このサイトについて",
    zh: "关于我们",
    top: "/about",
    items: []
  }
]

export const useNavigation = () => {
  return useState<Array<NavCategory>>("navs", () => navs)
}