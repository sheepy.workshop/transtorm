import type { IncomingMessage, ServerResponse } from "http"
import fetch from "node-fetch"
import { useBody } from 'h3'

export default async (req: IncomingMessage, res: ServerResponse) => {
  const body = await useBody(req)
  console.log(7)
  console.log(body)
  return new Promise((resolve, reject) => {
    const endpoint = `https://shortie.sh/api/rest/redirect`;
    fetch(endpoint, {method: "POST", body, headers: {"Content-Type": "application/json"} }).then(shorten => {
        console.log(10)
        console.log(shorten)
      if (shorten.ok) {
        shorten.text()
          .then(text => {
            resolve(text)
          })
          .catch((parseerr) => {
            reject(parseerr)
          });
      } else {
        console.log(19)
        console.log(JSON.stringify(shorten))
        reject(shorten.status)
      }
    })
      .catch((fetcherr) => {
        reject(fetcherr)
      });
  })
}