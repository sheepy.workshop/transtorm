import type { IncomingMessage, ServerResponse } from "http"
import fetch from "node-fetch"
import config from '#config'
import Airtable, { FieldSet } from "airtable"

export default async (req: IncomingMessage, res: ServerResponse) => {
  const rcds = []
  const base = new Airtable({
    apiKey: config.airtable
  }).base("appq74F7uMn3eCUCh")
  return new Promise((resolve, reject) => {
    base("Table 1").select({
      maxRecords: 20,
      view: "Grid view",
    }).firstPage((err, records) => {
      if (err) {
        reject(err)
      } else {
        const fields: FieldSet[] = []
        records.forEach(rcd => {
          fields.push(rcd.fields)
        })
        fields.sort(() => Math.random() - 0.5)
        resolve(fields)
      }
    });
  })
}

// const endpoint = "https://api.airtable.com/v0/appq74F7uMn3eCUCh/Table%201"
// const maxRecs = 20
// const group = 1
// const filter = `filterByFormula=({randamize}=${group})`
// const maxSize = `maxRecords=${maxRecs}`
// const others = "view=Grid%20view"
// const query = `${endpoint}?auth_key=${config.airtable}&${filter}&${maxSize}&${others}`
// const headers = {
//   "Authorization": `Bearer ${config.airtable}`
// }
// console.log(headers)
// // const stormcards: StormCard[] = [{
// //   ja: "aaa",
// //   zh: "bbb",
// //   author: "me",
// //   randamize: 1
// // }]
// const stormcards: StormCard[] = []
// const cards = ref<StormCard[]>(stormcards)

// // fetch(query, { headers })
// fetch(query)
//   .then(res => {
//     if (res.ok) {
//       res.json()
//         .then(j => {
//           j.records.forEach(rcd => {
//             cards.value.push(rcd.fields)
//             console.log(cards.value[0])
//             isLoaded.value = true
//           })
//         })
//     } else {
//       console.log(res)
//     }
//   })