import type { IncomingMessage, ServerResponse } from "http"
import fetch from "node-fetch"
import { useQuery, useBody } from 'h3'
import config from '#config'

export default async (req: IncomingMessage, res: ServerResponse) => {
  // const { text = "", src_lang, tgt_lang } = useQuery(req)
  const { text = "", src_lang, tgt_lang } = await useBody(req)
  // const query = useQuery(req)
  // console.log(query)
  // console.log(JSON.stringify(query))
  // console.log(text.toString())
  return new Promise((resolve, reject) => {
    const endpoint = `https://api-free.deepl.com/v2/translate?auth_key=${config.deepl}&text=${text}&source_lang=${src_lang}&target_lang=${tgt_lang}`;
    // console.log(endpoint)
    fetch(endpoint).then((deeplres) => {
      if (deeplres.ok) {
        deeplres.json()
          .then((translated: any) => {
            resolve(translated.translations[0].text)
          })
          .catch((deeplerr) => {
            console.log("res not ok")
            console.log(deeplerr)
            reject(deeplerr)
          });
      } else {
        console.log(JSON.stringify(deeplres))
        reject(deeplres.ok)
      }
    })
      .catch((fetcherr) => {
        reject(fetcherr)
      });
  })
}