import type { IncomingMessage, ServerResponse } from "http"
import fetch from "node-fetch"
import config from '#config'
import Airtable from "airtable"
import { useBody } from 'h3'

export default async (req: IncomingMessage, res: ServerResponse) => {
  const body = await useBody(req) as Omit<StormCard, "No">
  const rcd = {
    fields: {
      ...body
    }
  }
  const base = new Airtable({
    apiKey: config.airtable
  }).base("appq74F7uMn3eCUCh")
  return new Promise((resolve, reject) => {
    base("Table 1").create([rcd], (err, records) => {
      if (err) {
        reject(err)
      } else {
        // console.log(records)
        resolve(records)
      }
    });
  })
}