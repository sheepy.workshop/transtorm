import type { IncomingMessage, ServerResponse } from "http"
// import fetch from "node-fetch"
import { useQuery } from 'h3'
import config from '#config'
import { createClient } from 'microcms-js-sdk'; //ES6

export default async (req: IncomingMessage, res: ServerResponse) => {
  // Initialize Client SDK.
  const client = createClient({
    serviceDomain: "transtormer", // YOUR_DOMAIN is the XXXX part of XXXX.microcms.io
    apiKey: config.microcms,
  });
  const { name } = useQuery(req)
  return new Promise((resolve, reject) => {
    client
      .get({
        endpoint: 'trace',
        queries: { filters: `name[equals]${name}` }
      })
      .then(res => resolve(res))
      .catch(err => reject(err))
  })
}
